package com.hackathon.hackathon.models;

import org.springframework.data.annotation.Id;

public class FilmModel {

    @Id
    private String id;
    private String desc;
    private float price;
    private String genre;

    public FilmModel() {
    }

    public FilmModel(String id, String desc, float price, String genre) {
        this.id = id;
        this.desc = desc;
        this.price = price;
        this.genre = genre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
