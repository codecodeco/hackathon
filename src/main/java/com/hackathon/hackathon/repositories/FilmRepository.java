package com.hackathon.hackathon.repositories;

import com.hackathon.hackathon.models.FilmModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FilmRepository extends MongoRepository<FilmModel, String> {
}
