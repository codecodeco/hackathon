package com.hackathon.hackathon.services;

import com.hackathon.hackathon.models.FilmModel;
import com.hackathon.hackathon.repositories.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService {

    @Autowired
    FilmRepository filmRepository;

    public List<FilmModel> findAll() {
        System.out.println("findAll en FilmService");

        return this.filmRepository.findAll();
    }

    public FilmModel add(FilmModel product) {
        System.out.println("add en FilmService");

        return this.filmRepository.save(product);
    }
}
