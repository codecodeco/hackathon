package com.hackathon.hackathon.controllers;

import com.hackathon.hackathon.models.ClientModel;
import com.hackathon.hackathon.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackaton")
public class ClientController {

    @Autowired
    ClientService userService;

    @GetMapping("/clients")
    public ResponseEntity<List<ClientModel>> getClients(@RequestParam(name = "$orderby", required = false) String orderBy) {
        System.out.println("getClients controller");
        return new ResponseEntity<>(
                this.userService.findAll(orderBy),
                HttpStatus.OK
        );
    }

    @GetMapping("/clients/{id}")
    public ResponseEntity<?> getClientsById(@PathVariable String id) {
        System.out.println("getClientsByID controller");
        System.out.println("La id del user a buscar es:" + id);

        Optional<ClientModel> res = this.userService.findById(id);
        return new ResponseEntity<>(
                res.isPresent() ? res.get() : "Cliente no encontrado",
                res.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/clients")
    public ResponseEntity<ClientModel> addClient(@RequestBody ClientModel clientModel) {
        System.out.println("addUser en UserController");
        System.out.println("La id del Cliente a crear es :" + clientModel.getId());
        System.out.println("La age del Cliente a crear es :" + clientModel.getAge());
        System.out.println("La name del producto a crear es :" + clientModel.getName());

        return new ResponseEntity<>(
                this.userService.addUser(clientModel),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/clients/{id}")
    public ResponseEntity<ClientModel> updateClient(@RequestBody ClientModel user, @PathVariable String id) {
        System.out.println("updateUser controller");
        System.out.println("La id del user modificado:" + id);
        System.out.println("La id del user a actualizar es:" + user.getId());
        System.out.println("La name del user a actualizar es:" + user.getName());
        System.out.println("La age del user a actualizar es:" + user.getAge());

        Optional<ClientModel> userUpdate = this.userService.findById(id);
        if (userUpdate.isPresent()) {
            System.out.println("User encotnrado, comenzamos a actualizar");
            this.userService.update(user);
        } else {
            System.out.println("User no encontrado, no actualizar");
        }
        return new ResponseEntity<>(
                user, userUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/clients/{id}")
    public ResponseEntity<String> deleteClient(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del user a borrar es:" + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "User borrado" : "User no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
