package com.hackathon.hackathon.controllers;

import com.hackathon.hackathon.models.FilmModel;
import com.hackathon.hackathon.services.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hackaton")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class FilmController {

    @Autowired
    FilmService filmService;

    @RequestMapping("/films")
    public ResponseEntity<List<FilmModel>> getFilms(){
        System.out.println("getFilms");

        return new ResponseEntity<>(
                this.filmService.findAll(),
                HttpStatus.OK
        );
    }

    @PostMapping("/films")
    public ResponseEntity<FilmModel> addFilm(@RequestBody FilmModel film){
        System.out.println("addFilm");
        System.out.println("La id de la film a crear es: " + film.getId());

        return new ResponseEntity<>(
                this.filmService.add(film),
                HttpStatus.CREATED
        );
    }
}
